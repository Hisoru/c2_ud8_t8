package dto;

public class Password {

	private int longitud;
	private String contrasena;
	
	// Constructor por defecto
	public Password() {
		
		this.longitud = 8;
		this.contrasena = "";
		
	}
	
	// Constructor con la longitud que nosotros le pasemos, generara una contrase�a aleatoria con esa longitud
	public Password(int longitud, String contrasena) {
		
		this.longitud = longitud;
		this.contrasena = contrasenaAleatoria(longitud);
		
	}
	
	// M�todo que genera una contrase�a aleatoria con la longitud introducida
	private static String contrasenaAleatoria(int longitud) {
		
		char[] array = new char[longitud];
		
		for (int i = 0; i < longitud; i++) {
			
			char numeroAleatorio = (char) (Math.random() * ((122 - 48) + 1) + 48);
			array[i] = numeroAleatorio;
			
		}
		
		String password = new String(array);
		
		return password;
		
	}

	@Override
	public String toString() {
		return "Password [longitud=" + longitud + ", contrasena=" + contrasena + "]";
	}
	
}
