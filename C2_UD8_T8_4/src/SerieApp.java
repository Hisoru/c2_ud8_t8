import dto.Serie;

public class SerieApp {

	public static void main(String[] args) {
		
		Serie s1 = new Serie();
		System.out.println(s1);
		
		Serie s2 = new Serie("The Mandalorian", "George Lucas");
		System.out.println(s2);
		
		Serie s3 = new Serie("The Mandalorian", 1, "Space Western", "George Lucas");
		System.out.println(s3);

	}

}
