package dto;

public class Serie {

	private String titulo;
	private int numeroDeTemporadas;
	private boolean entregado;
	private String genero;
	private String creador;
	
	// Constructor por defecto
	public Serie() {
		
		this.titulo = "";
		this.numeroDeTemporadas = 3;
		this.entregado = false;
		this.genero = "";
		this.creador = "";
		
	}
	
	// Constructor con el t�tulo y creador, el resto por defecto
	public Serie(String titulo, String creador) {
		
		this.titulo = titulo;
		this.numeroDeTemporadas = 3;
		this.entregado = false;
		this.genero = "";
		this.creador = creador;
		
	}
	
	// Constructor con todos los atributos, excepto de entregado.
	public Serie(String titulo, int numeroDeTemporadas, String genero, String creador) {
		
		this.titulo = titulo;
		this.numeroDeTemporadas = numeroDeTemporadas;
		this.genero = genero;
		this.creador = creador;
		
	}

	@Override
	public String toString() {
		return "Serie [titulo=" + titulo + ", numeroDeTemporadas=" + numeroDeTemporadas + ", entregado=" + entregado
				+ ", genero=" + genero + ", creador=" + creador + "]";
	}
	
}
