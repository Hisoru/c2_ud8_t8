package dto;

public class Electrodomestico {

	private double precioBase;
	private String color;
	private char consumoEnergetico;
	private double peso;
	
	// Constructor por defecto
	public Electrodomestico() {
		
		this.precioBase = 100;
		this.color = "Blanco";
		this.consumoEnergetico = 'F';
		this.peso = 5;
		
	}
	
	// Constructor con el precio y peso, el resto por defecto
	public Electrodomestico(double precioBase, double peso) {
		
		this.precioBase = precioBase;
		this.color = "Blanco";
		this.consumoEnergetico = 'F';
		this.peso = peso;
		
	}
	
	// Constructor con todos los atributos
	public Electrodomestico(double precioBase, String color, char consumoEnergetico, double peso) {
		
		this.precioBase = precioBase;
		this.color = color(color);
		this.consumoEnergetico = consumoEnergetico(consumoEnergetico);
		this.peso = peso;
		
	}
	
	// M�todo que comprueba si el color introducido es correcto
	public static String color(String color) {
		
		if (color.equalsIgnoreCase("Blanco") || color.equalsIgnoreCase("Negro") || color.equalsIgnoreCase("Rojo") ||
				color.equalsIgnoreCase("Azul") || color.equalsIgnoreCase("Gris")) {
		
			return color;
			
		} else {
			
			return "Blanco";
			
		}
		
	}
	
	// M�todo que comprueba si el consumo energ�tico introducido es correcto
	public static char consumoEnergetico(char consumoEnergetico) {
		
		if (consumoEnergetico == 'A' || consumoEnergetico == 'B' || consumoEnergetico == 'C' ||
				consumoEnergetico == 'D' || consumoEnergetico == 'F') {
		
			return consumoEnergetico;
			
		} else {
			
			return 'F';
			
		}
		
	}

	@Override
	public String toString() {
		return "Electrodomestico [precioBase=" + precioBase + ", color=" + color + ", consumoEnergetico="
				+ consumoEnergetico + ", peso=" + peso + "]";
	}
	
}
