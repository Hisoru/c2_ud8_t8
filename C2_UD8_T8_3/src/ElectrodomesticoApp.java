import dto.Electrodomestico;

public class ElectrodomesticoApp {

	public static void main(String[] args) {
		
		Electrodomestico e1 = new Electrodomestico();
		System.out.println(e1);
		
		Electrodomestico e2 = new Electrodomestico(12.3, 45.6);
		System.out.println(e2);
		
		Electrodomestico e3 = new Electrodomestico(12.3, "Marron", 'L', 45.6);
		System.out.println(e3);

	}

}
