package dto;

public class Persona {
	
	private String nombre;
	private int edad;
	private String dni;
	final private char SEXO;
	private double peso;
	private double altura;
	
	// Constructor por defecto
	public Persona() {
		
		this.nombre = "";
		this.edad = 0;
		this.dni = "";
		this.SEXO = 'H';
		this.peso = 0;
		this.altura = 0;
		
	}
	
	// Constructor con el nombre, edad y sexo, el resto por defecto
	public Persona(String nombre, int edad, char sexo) {
		
		this.nombre = nombre;
		this.edad = edad;
		this.dni = "";
		this.SEXO = sexo;
		this.peso = 0;
		this.altura = 0;
		
	}
	
	// Constructor con todos los atributos como parámetro
	public Persona(String nombre, int edad, String dni, char sexo, double peso, double altura) {
		
		this.nombre = nombre;
		this.edad = edad;
		this.dni = dni;
		this.SEXO = sexo;
		this.peso = peso;
		this.altura = altura;
		
	}

	@Override
	public String toString() {
		return "Persona [nombre=" + nombre + ", edad=" + edad + ", dni=" + dni + ", SEXO=" + SEXO + ", peso=" + peso
				+ ", altura=" + altura + "]";
	}
	
}
