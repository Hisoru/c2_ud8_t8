import dto.Persona;

public class PersonaApp {

	public static void main(String[] args) {
		
		Persona p1 = new Persona();
		System.out.println(p1);
		
		Persona p2 = new Persona("Daniel", 23, 'H');
		System.out.println(p2);
		
		Persona p3 = new Persona("Daniel", 23, "39942253Q", 'H', 160, 180);
		System.out.println(p3);

	}

}
